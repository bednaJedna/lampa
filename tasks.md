# Lampa logs processor

## Python version
Trying 3.4 - should be OK, since using only basic and native packages

## To-Do

1. Consolidate code files into one .py file
2. Rewrite paths, so input loaded in getcwd() and output in dyn. created dir
2. Take .csv files and create plots (mathplotlib?), save into dyn. created dir

## Plots

1. pro *input.txt_1.csv:
    - udelat histogram pro sloupec temp viz priloha
    - do popisku pridat celkovy cas v sekundach(vynasobit hodnotu timestep a  run , jednotky pro units metal). Gauss moc nesedi, mel by to byt spis Maxwell–Boltzmann.
2. pro input.txt_2.csv:
    - histogram sloupce c_eatoms
    - vyvoj eregie a tlaku: graf step vs c_eatoms, step vs press (podle prehlednosti muzes i do jednoho, napr pomoci twinx)
    - Note: c_eatoms - jednotky v eV, press - tlak v barech

