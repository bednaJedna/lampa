import argparse
import csv
from os import getcwd, chdir, mkdir, listdir
from os.path import isdir
from time import sleep

import matplotlib.pyplot as plt
from tqdm import tqdm

VALUES = ['units', 'fix', 'timestep', 'run', 'total wall time']
PLOTS = dict(
    temp=['histogram'],
    c_eatoms=['histogram', 'line'],     # x-axis: steps
    press=['line']                      # x-axis: steps
)
OUTPUT_DATA = './outputs_data/'
OUTPUT_PLOTS = './outputs_plots/'

# Argparsing module
parser = argparse.ArgumentParser()
group_files = parser.add_argument_group('log_files')
group_files.add_argument(
    '-log',
    '--log',
    type=str,
    help='Filename of the log file, e.g. "logfile.txt". \n You can add multiple files' +\
         ' separated by space, e.g. "-log [filename01.txt] -log[filename02.txt], etc.' +\
         ' You MUST specify at least ONE file.',
    required=True,
    action='append'
)
args = parser.parse_args()


def load_file(filename: str):
    '''
    Loads filename, which was specified in commandline
    :param filename:
    :return nested list:
    '''
    with open('{}'.format(filename), mode='r', newline='') as file:
        output = file.readlines()

    return output


def load_csv(filename: str):
    '''
    Loads csv file
    :param filename:
    :return nested list:
    '''
    with open(
            ''.join([OUTPUT_DATA, '{}'.format(filename)]),
            mode='r',
            newline='') as file:
        reader = csv.reader(file, delimiter=',')
        data = list()
        for row in reader:
            data.append(row)

        return data


def get_serie(data: list, columnheader: str):
    '''
    Extracts specified "column" from nested lists of .csv data.
    Using this crude method since want to avoid installing third-party
    packages like pandas.
    Row[0] is header
    row[1:] is data
    :params: data, column
    :return:
    '''
    output = list()
    headers = data[0]
    i = headers.index(columnheader)

    for x, row in enumerate(data):
        if x == 0:
            output.append(row[i])
        else:
            output.append(float(row[i]))

    return output


def process_log(filename: str):
    '''
    Opens given log file and process data.
    Returns nested list for  with needed data for saving into csv
    :param filename:
    :return tuple:
    '''
    file = load_file(filename)

    # GET requested values
    values = dict()

    # print('Processing logs to get needed values... \n')

    for value in tqdm(VALUES, desc='Processing values', unit='value'):
        for line in tqdm(file, desc='Processing lines', unit='line'):
            line = line.lower().strip()

            if line.startswith(value):
                if line.startswith('total wall time'):
                    temp = line.split(sep=' ', maxsplit=3)[3]
                else:
                    temp = line.split(sep=' ', maxsplit=1)[1]

                if value not in values.keys():
                    values[value] = temp.strip()
                else:
                    values[value] += ''.join([', ', temp.strip()])

            else:
                continue
    # GET data
    counter = 1
    read_data = False
    data = dict()
    name = str()

    # print('Processing logs to get needed data... \n')

    for line in tqdm(file, desc='Processing lines', unit='line'):
        line = line.lower().strip()

        if line.startswith('step temp'):
            name = 'data_{}_{}.csv'.format(filename, counter)
            counter += 1
            read_data = True

            # Header
            data[name] = list()
            data[name].append(line.replace(' ', ',').split(sep=','))

        elif read_data and (line.startswith('loop time') is False):

            # Data
            temp = line.replace(' ', ',').split(sep=',')
            temp = [each for each in temp if each is not '']

            data[name].append(temp)

        else:
            read_data = False
            continue

    return values, data


def save_data(data: tuple, filename: str):
    '''
    Saves data into .csv files
    :params data, filename:
    :return:
    '''
    values_ = data[0]
    data_ = data[1]

    # IF output dir does not exist, create it
    if isdir(OUTPUT_DATA) is False:
        mkdir(OUTPUT_DATA)

    # print('Saving data ....')

    # Save Data into .csv file
    for keys, values in tqdm(data_.items(), desc='Processing data', unit='indicator'):
        with open(
                ''.join([OUTPUT_DATA, '{}'.format(keys)]),
                mode='w',
                newline='') as file:
            writer = csv.writer(file, delimiter=',')
            for row in tqdm(values, desc='Writing data into csv', unit='data row'):
                writer.writerow(row)

    # print('Saving values ....')

    # Save values into .csv file
    header = [each for each in values_.keys()]
    figs = [each for each in values_.values()]

    with open(
            ''.join([OUTPUT_DATA, '{}_{}.csv'.format(filename, 'values')]),
            mode='w',
            newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(header)
        writer.writerow(figs)


def create_histogram(data: list, datafile: str, outputplotdir: str):
    '''
    Creates histogram plot and saves it to designated output folder
    :params: data, outputplotdir
    :return:
    '''
    # print('Creating histogram for "{}" from file "{}"...'.format(data[0], datafile))

    n, bins, patches = plt.hist(data[1:], 20, density=True,
                                facecolor='b', alpha=0.75)
    plt.xlabel(data[0])
    plt.ylabel('Probability')
    plt.title('Histogram of "{}" from file "{}"'.format(data[0], datafile))
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(''.join([
        outputplotdir,
        'hist_',
        data[0],
        '_',
        datafile,
        '.jpg']
        ),
        format='jpeg')
    # Things need to be slowed down to properly close and save the fig.
    # Kudos to Stack Overflow :)
    sleep(1)
    plt.close()
    plt.gcf()


def create_line(xaxis: list, yaxis: list, datafile: str, outputplotdir: str):
    '''
    Creates line plot and saves it to designated output folder.
    :param data:
    :param datafile:
    :param outputplotdir:
    :return:
    '''
    # print('Creating line-plot for "{}" from file "{}"...'.format(
    #     yaxis[0], datafile))

    plt.plot(xaxis[1:], yaxis[1:], color='b', linestyle='-')
    plt.xlabel(xaxis[0])
    plt.ylabel(yaxis[0])
    plt.title('Line-plot of "{}" from file "{}"'.format(yaxis[0], datafile))
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(''.join([
        outputplotdir,
        'line_',
        yaxis[0],
        '_',
        datafile,
        '.jpg']
    ),
        format='jpeg')
    sleep(1)
    plt.close()
    plt.gcf()


def create_plots(outputplotdir: str):
    '''
    Processes output .csv files and creates plots from them
    :param: outputdatadir
    :return:
    '''
    # CHECK, that dir exists
    if isdir(outputplotdir) is False:
        mkdir(outputplotdir)

    # CHECK, that datadir is not empty
    dflist = listdir(OUTPUT_DATA)

    if len(dflist) > 0:
        # LOOP through list of files
        # PROCESS only those starting with "data_"
        for f in tqdm(dflist, desc='Looping through csv files', unit='file'):
            if 'data_' in f:
                # LOAD .csv datafile
                data = load_csv(f)
                # LOOP through PLOT dict:
                # keys: identify headers in csv file
                # values: identify type of plot to create
                # WARNING: values are in lists, even if for a single value
                #           to simplify processing alg.
                for header, plots in tqdm(PLOTS.items(), desc='Processing data into plots', unit='indicator'):
                    for plot in tqdm(plots, desc='Creating plots', unit='plot'):
                        if plot == 'histogram':
                            # LOAD needed data-serie
                            col = get_serie(data, header)

                            create_histogram(col, f, outputplotdir)

                        elif plot == 'line':
                            xaxis = get_serie(data, 'step')    # xaxis[0] is string
                            yaxis = get_serie(data, header) # yaxis[0] is string

                            create_line(xaxis, yaxis, f, outputplotdir)

                        else:
                            continue

            else:
                continue


# Run the script
if __name__ == '__main__':

    chdir(getcwd())

    for parameter in tqdm(args.log, desc='Processing log files', unit='file'):
        save_data(process_log(parameter), parameter)
        create_plots(OUTPUT_PLOTS)
        print('\n\n\n')

