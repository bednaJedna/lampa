## Log processor - how to
1. Written for Win10, Linux (Ubuntu), Python 3.6 +
2. External packages: 
    - matplotlib
    - tqdm


**How to run**

1. Switch to root directory _./lampa_
2. run script
    - in Win10: _py run.py -log [PATH][filename.txt]_
    - in Linux: _python3 run.py -log [PATH][filename.txt]_
    - it is possible to process multiple files at once:
        - e.g. _py run.py -log [PATH][filename01.txt] -log [PATH][filename02.txt]_

3. output files are saved into _./outputs_plots_

**DO NOT** run main script from other folders. The relative paths would
not work.
